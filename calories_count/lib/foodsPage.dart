import 'dart:html';

import 'package:calories_count/food_add_form.dart';
import 'package:calories_count/foods_information.dart';
import 'package:calories_count/dashboard.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class foodsPage extends StatefulWidget {
  foodsPage({Key? key}) : super(key: key);

  @override
  _foodsPageState createState() => _foodsPageState();
}

class _foodsPageState extends State<foodsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Select Your Foods'),
      ),
      body: Container(
        child: foodInformation(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          Navigator.push(context, MaterialPageRoute(builder: (context) => foodAddForm(foodId: '')));
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}