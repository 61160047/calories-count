import 'dart:html';

import 'package:calories_count/activity_information.dart';
import 'package:calories_count/food_add_form.dart';
import 'package:calories_count/foods_information.dart';
import 'package:calories_count/dashboard.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class activityPage extends StatefulWidget {
  activityPage({Key? key}) : super(key: key);

  @override
  _activityPageState createState() => _activityPageState();
}

class _activityPageState extends State<activityPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Your Activity'),
      ),
      body: Container(
        child: activityInformation(),
      ),
    );
  }
}