import 'dart:html';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class workoutAddForm extends StatefulWidget {
  String workoutId;
  workoutAddForm({Key? key, required this.workoutId}) : super(key: key);

  @override
  _workoutAddFormState createState() => _workoutAddFormState(workoutId);
}

class _workoutAddFormState extends State<workoutAddForm> {
  String workoutId;
  String workoutName = "";
  int workoutCalories = 0;
  String workoutUnit = "";
  int workoutQuantity = 0;
  var totalcnq = 0.0;
  CollectionReference workout = FirebaseFirestore.instance.collection('work_out');
  _workoutAddFormState(this.workoutId);
  TextEditingController _workoutNameController = new TextEditingController();
  TextEditingController _workoutCaloriesController = new TextEditingController();
  TextEditingController _workoutUnitController = new TextEditingController();
  TextEditingController _workoutQuantityController = new TextEditingController();

  void initState() {
    super.initState();
    if (this.workoutId.isNotEmpty) {
      workout.doc(this.workoutId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          setState(() {
            workoutName = data['workout_name'];
            workoutCalories = data['workout_calories'];
            workoutUnit = data['workout_unit'];
            workoutQuantity = data['workout_quantity'];
          });
          _workoutNameController.text = workoutName;
          _workoutCaloriesController.text = workoutCalories.toString();
          _workoutUnitController.text = workoutUnit;
          _workoutQuantityController.text = workoutQuantity.toString();
        }
      });
    }
  }

  final _formkey = GlobalKey<FormState>();

  Future<void> addWorkout() {
    List<String> splitList = workoutName.split(' ');
    List<String> indexList = [];

    for(int i=0;i<splitList.length;i++){
      for(int j=0;j<splitList[i].length+i;j++){
        indexList.add(splitList[i].substring(0,j).toLowerCase());
      }
    }

    return workout
        .add({
          'workout_name': this.workoutName,
          'workout_calories': this.workoutCalories,
          'workout_quantity': this.workoutQuantity,
          'workout_unit': this.workoutUnit,
          'searchIndex': indexList
        })
        .then((value) => print('workout added'))
        .catchError((error) => print('Failed to add workout: $error'));
  }

  void totalCalAndQuan() {
    if (workoutQuantity <= 9) {
      totalcnq = (workoutCalories * workoutQuantity) as double;
    } else {
      totalcnq = (workoutCalories * (workoutQuantity / workoutQuantity)) as double;
    }
    print(totalcnq);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Workout'),
      ),
      body: Form(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        key: _formkey,
        child: Container(
          padding: EdgeInsets.all(16),
          child: ListView(
            children: [
              TextFormField(
                controller: _workoutNameController,
                //initialValue: fullname,
                decoration: InputDecoration(labelText: 'Workout name'),

                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input workout name';
                  }
                  return null;
                },
                onChanged: (value) {
                  setState(() {
                    workoutName = value;
                  });
                },
              ),
              TextFormField(
                controller: _workoutCaloriesController,
                //initialValue: fullname,
                decoration: InputDecoration(labelText: 'Workout Calories'),

                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input workout calories';
                  }
                  return null;
                },
                onChanged: (value) {
                  setState(() {
                    workoutCalories = int.tryParse(value)!;
                  });
                },
              ),
              TextFormField(
                controller: _workoutQuantityController,
                //initialValue: fullname,
                decoration: InputDecoration(labelText: 'Workout quantity'),

                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input workout quantity';
                  }
                  return null;
                },
                onChanged: (value) {
                  setState(() {
                    workoutQuantity = int.tryParse(value)!;
                  });
                },
              ),
              TextFormField(
                controller: _workoutUnitController,
                //initialValue: fullname,
                decoration: InputDecoration(labelText: 'Workout unit'),

                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input workout unit';
                  }
                  return null;
                },
                onChanged: (value) {
                  setState(() {
                    workoutUnit = value;
                  });
                },
              ),
              ButtonBar(
                alignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text('Cancel')),
                  ElevatedButton(
                      onPressed: () async {
                        if (_formkey.currentState!.validate()) {
                          //if(foodId.isEmpty) {
                          await addWorkout();
                          //totalCalAndQuan();
                          //}

                          Navigator.pop(context);
                        }
                      },
                      child: const Text('Save')),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
