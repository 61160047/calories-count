import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

class ProfileWidget extends StatefulWidget {
  ProfileWidget({Key? key}) : super(key: key);

  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends State<ProfileWidget> {
  final _formKey = GlobalKey<FormState>();
  String name = '';
  int weight = 0, hight = 0;
  String gender = 'M';
  int d = 0, m = 0, y = 0, day = 0, month = 0, year = 0;

  final _nameController = TextEditingController();
  final _weightController = TextEditingController();
  final _hightController = TextEditingController();
  final _ageController = TextEditingController();
  final _dateController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _loadProfile();
  }

  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('name') ?? '';
      _nameController.text = name;
      weight = prefs.getInt('weight') ?? 0;
      _weightController.text = '$weight';
      hight = prefs.getInt('hight') ?? 0;
      _hightController.text = '$hight';
      gender = prefs.getString('gender') ?? 'M';
      year = prefs.getInt('year') ?? 0;
      _ageController.text = '$year';
    });
  }

  Future<void> _saveProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('name', name);
      prefs.setInt('weight', weight);
      prefs.setInt('hight', hight);
      prefs.setString('gender', gender);
      prefs.setInt('year', year);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
      ),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(16),
          child: ListView(
            children: [
              TextFormField(
              
                autofocus: true,
                controller: _nameController,
                validator: (value) {
                  if (value == null || value.isEmpty || value.length < 2) {
                    return 'กรุณาใส่ชื่อ';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'ชื่อ-นามสกุล',hintText: 'กขค'),
                onChanged: (value) {
                  setState(() {
                    name = value;
                  });
                },
              ),
              TextFormField(
                controller: _weightController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาใส่น้ำหนัก';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'น้ำหนัก',hintText: '0'),
                onChanged: (value) {
                  setState(() {
                    weight = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              TextFormField(
                controller: _hightController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาใส่ส่วนสูง';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'ส่วนสูง'),
                onChanged: (value) {
                  setState(() {
                    hight = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              TextFormField(
                controller: _dateController,
                decoration: InputDecoration(
                  labelText: "Date of birth",
                  hintText: "Ex. Insert your dob",
                ),
                onTap: () async {
                  //DateTime date = DateTime(1900);
                  FocusScope.of(context).requestFocus(new FocusNode());

                  DateTime date = (await showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(1900),
                      lastDate: DateTime(2100)))!;
                  setState(() {
                    d = int.parse(DateFormat("dd").format(date));
                    int presentDay =
                        int.parse(DateFormat("dd").format(DateTime.now()));
                    m = int.parse(DateFormat("MM").format(date));
                    int presentMonth =
                        int.parse(DateFormat("MM").format(DateTime.now()));
                    y = int.parse(DateFormat("yyyy").format(date));
                    int presentYear =
                        int.parse(DateFormat("yyyy").format(DateTime.now()));
                    day = presentDay - d;
                    month = presentMonth - m;
                    year = presentYear - y;

                    if (day < 0 || month < 0) {
                      year--;
                    }
                  });
                  _dateController.text = date.toString();
                },
              ),
              DropdownButtonFormField(
                value: gender,
                items: [
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        Icon(Icons.male),
                        SizedBox(
                          width: 8,
                        ),
                        Text('ชาย')
                      ],
                    ),
                    value: 'M',
                  ),
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        Icon(Icons.female),
                        SizedBox(
                          width: 8,
                        ),
                        Text('หญิง')
                      ],
                    ),
                    value: 'F',
                  ),
                ],
                onChanged: (String? newValue) {
                  setState(() {
                    gender = newValue!;
                  });
                },
              ),
              ButtonBar(
                alignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text('Cancel')),
                  ElevatedButton(
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          _saveProfile();
                          Navigator.pop(context);
                        }
                      },
                      child: const Text('Save')),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
