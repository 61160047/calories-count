import 'dart:html';

import 'package:calories_count/activityPage.dart';
import 'package:calories_count/activity_information.dart';
import 'package:calories_count/food_select_form.dart';
import 'package:calories_count/foods_information.dart';
import 'package:calories_count/profile.dart';
import 'package:calories_count/workoutPage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'foodsPage.dart';

class MyApp extends StatefulWidget {
  MyApp({
    Key? key,
  }) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();
  CollectionReference foods_select =
      FirebaseFirestore.instance.collection('foods_select');
  TextEditingController _foodcallController = new TextEditingController();
  String name = '';
  int weight = 0;
  int hight = 0;
  String gender = '-';
  int year = 0;

  /*int cal = 0;
  int quan = 0;
  var caleatperday = 0.0;
  var total = 0.0;*/

  var caloriesperDay = 0.0;
  var selectfoods = [];

  @override
  void initState() {
    super.initState();
    _loadProfile();
  }

  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('name') ?? '';
      weight = prefs.getInt('weight') ?? 0;
      hight = prefs.getInt('hight') ?? 0;
      gender = prefs.getString('gender') ?? '-';
      year = prefs.getInt('year') ?? 0;
    });
  }

  Future<void> _loadCalories() async {
    var prefs = await SharedPreferences.getInstance();
    var gndr = prefs.getString('gender') ?? '-';
    var wght = prefs.getInt('weight') ?? 0;
    var hght = prefs.getInt('hight') ?? 0;
    var age = prefs.getInt('year') ?? 0;
    setState(() {
      caloriesperDay = 0.0;
      if (gndr == 'M') {
        caloriesperDay = (66 + (13.7 * wght) + (5 * hght) - (6.8 * age));
      } else {
        caloriesperDay = (665 + (9.6 * wght) + (1.8 * hght) - (4.7 * age));
      }
    });
    //total = caloriesperDay - caleatperday;
  }

  Future<void> _resetProfile() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('name');
    prefs.remove('weight');
    prefs.remove('hight');
    prefs.remove('gender');
    prefs.remove('year');
    caloriesperDay = 0;
    await _loadProfile();
  }

  Widget _datapeople() {
    return Scaffold(
      appBar: AppBar(title: Text('Main')),
      drawer: Container(
        width: 200,
        color: Colors.blue,
        child: ListView(
          children: [
            Padding(padding: const EdgeInsets.all(1)),
            Icon(Icons.menu),
            //Padding(padding: const EdgeInsets.all(10)),
            ListTile(
              leading: Icon(
                Icons.account_circle_rounded,
                color: Colors.black,
              ),
              title: Text('Profile'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ProfileWidget()));
                await _loadProfile();
                await _loadCalories();
              },
            ),
            ListTile(
              leading: Icon(
                Icons.food_bank,
                color: Colors.black,
              ),
              title: Text('Foods'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => foodsPage()));
              },
            ),
            ListTile(
              leading: Icon(
                Icons.fitness_center,
                color: Colors.black,
              ),
              title: Text('Work out'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => workoutPage()));
              },
            ),
            ListTile(
              leading: Icon(
                Icons.local_activity,
                color: Colors.black,
              ),
              title: Text('Activities'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => activityPage()));
              },
            ),
          ],
        ),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView(
              children: <Widget>[
                Card(
                  clipBehavior: Clip.antiAlias,
                  child: Column(
                    children: [
                      ListTile(
                        leading: Icon(Icons.person,color: Colors.black,),
                        title: Text(name),
                        subtitle: Text(
                          'น้ำหนัก: $weight กก., ส่วนสูง: $hight ซม., อายุ: $year ปี, เพศ: $gender',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 12),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5),
                        child: Text(
                          'พลังงานที่ควรได้รับ',
                          style: TextStyle(fontSize: 11),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(1),
                        child: Text(
                          '$caloriesperDay',
                        ),
                      ),
                      ButtonBar(
                        alignment: MainAxisAlignment.end,
                        children: [
                          TextButton(
                            onPressed: () async {
                              await _resetProfile();
                            },
                            child: const Text('Reset'),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Expanded(child: activityInformation())
        ],
      ),
      /*Container(
        child: ListView(
          children: <Widget>[
                Card(
                  clipBehavior: Clip.antiAlias,
                  child: Column(
                    children: [
                      ListTile(
                        leading: Icon(Icons.person),
                        title: Text(name),
                        subtitle: Text(
                          'น้ำหนัก: $weight กก., ส่วนสูง: $hight ซม., อายุ: $year ปี, เพศ: $gender',
                          style: TextStyle(
                              color: Colors.black.withOpacity(0.6),
                              fontSize: 12),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5),
                        child: Text(
                          'พลังงานที่ควรได้รับ',
                          style: TextStyle(fontSize: 11),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(1),
                        child: Text(
                          '$caloriesperDay',
                        ),
                      ),
                      ButtonBar(
                        alignment: MainAxisAlignment.end,
                        children: [
                          TextButton(
                            onPressed: () async {
                              await _resetProfile();
                            },
                            child: const Text('Reset'),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                
              ],
        ),
      ),*/
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: _datapeople(),
      ),
    );
  }
}
