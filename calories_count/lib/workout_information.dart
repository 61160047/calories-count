import 'dart:html';

import 'package:calories_count/dashboard.dart';
import 'package:calories_count/food_select_form.dart';
import 'package:calories_count/food_add_form.dart';
import 'package:calories_count/workout_select_form.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class workOutInformation extends StatefulWidget {
  workOutInformation({Key? key}) : super(key: key);

  @override
  _workOutInformationState createState() => _workOutInformationState();
}

class _workOutInformationState extends State<workOutInformation> {
  TextEditingController textEditingController = TextEditingController();
  final Stream<QuerySnapshot> _workoutStream =
      FirebaseFirestore.instance.collection('work_out').snapshots();
  String searchString = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
              child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(15),
                child: Container(              
                  child: TextField(           
                    onChanged: (val) {
                      setState(() {
                        searchString = val;
                      });
                    },
                    controller: textEditingController,
                    decoration: InputDecoration(
                      hintText: 'Search work out here',
                      hintStyle: TextStyle(color: Colors.black),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: StreamBuilder(
                    stream: (searchString.trim() == '')
                        ? FirebaseFirestore.instance
                            .collection('work_out')
                            .snapshots()
                        : FirebaseFirestore.instance
                            .collection('work_out')
                            .where('searchIndex', arrayContains: searchString)
                            .snapshots(),
                    builder: (BuildContext context,
                        AsyncSnapshot<QuerySnapshot> snapshot) {
                      if (snapshot.hasError) {
                        return Text('Something went wrong');
                      }
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Text('Loading..');
                      }
                      switch (snapshot.connectionState) {
                        case ConnectionState.done:
                          return Text('We are done');
                        default:
                          return ListView(
                            children: snapshot.data!.docs
                                .map((DocumentSnapshot document) {
                              Map<String, dynamic> data =
                                  document.data()! as Map<String, dynamic>;
                              return Card(
                                child: Column(
                                  children: [
                                    ListTile(
                                      leading: Text(
                                        data['workout_name'],
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 30),
                                      ),
                                      title: Text(
                                        data['workout_calories'].toString() +
                                            " kcal",
                                        style: TextStyle(
                                            color: Colors.black,
                                            //fontWeight: FontWeight.bold,
                                            fontSize: 20),
                                      ),
                                      subtitle: Text(
                                        data['workout_quantity'].toString() +
                                            " " +
                                            data['workout_unit'],
                                        style: TextStyle(
                                            color: Colors.black,
                                            //fontWeight: FontWeight.bold,
                                            fontSize: 15),
                                      ),
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    workoutSelectForm(
                                                      workoutId: document.id,
                                                    )));
                                      },
                                    )
                                  ],
                                ),
                              );
                            }).toList(),
                          );
                      }
                    }),
              ),
            ],
          ))
        ],
      ),
    );
  }
}
