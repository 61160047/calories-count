import 'dart:html';

import 'package:calories_count/food_select_form.dart';
import 'package:calories_count/food_add_form.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class activityInformation extends StatefulWidget {
  activityInformation({Key? key}) : super(key: key);

  @override
  _activityInformationState createState() => _activityInformationState();
}

class _activityInformationState extends State<activityInformation> {
  final Stream<QuerySnapshot> _activityStream =
      FirebaseFirestore.instance.collection('activity').snapshots();
  CollectionReference activity = FirebaseFirestore.instance.collection('activity');

  Future<void> delActivity(activityId) {
    return activity
        .doc(activityId)
        .delete()
        .then((value) => print('Activity Delete'))
        .catchError((error) => print('Failed to dalete Activity: $error'));
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _activityStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text('Loading..');
          }
          return ListView(
            children: snapshot.data!.docs.map((DocumentSnapshot document) {
              Map<String, dynamic> data =
                  document.data()! as Map<String, dynamic>;
              return  Card(
                child: Column(
                  children: [
                    ListTile(
                      leading: Text(
                        data['name'],
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 30),
                      ),
                      title: Text(
                        data['calories'].toString() + " kcal",
                        style: TextStyle(
                            color: Colors.black,
                            //fontWeight: FontWeight.bold,
                            fontSize: 20),
                      ),
                      subtitle: Text(
                        data['quantity'].toString() +
                            " " +
                            data['unit'],
                        style: TextStyle(
                            color: Colors.black,
                            //fontWeight: FontWeight.bold,
                            fontSize: 15),
                      ),
                      /*onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => foodFormtest(
                                      foodId: document.id,
                                )
                                
                                ));
                      },*/
                      trailing: IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: () async {
                          await delActivity(document.id);
                         
                        },
                      ),
                    )
                  ],
                ),
              );             
            }).toList(),
          );
        });
  }
}

