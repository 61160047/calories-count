import 'dart:html';
import 'dart:js';

import 'package:calories_count/food_select_form.dart';
import 'package:calories_count/food_add_form.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class foodInformation extends StatefulWidget {
  foodInformation({Key? key}) : super(key: key);

  @override
  _foodInformationState createState() => _foodInformationState();
}

class _foodInformationState extends State<foodInformation> {
  TextEditingController textEditingController = TextEditingController();
  final Stream<QuerySnapshot> _foodStream =
      FirebaseFirestore.instance.collection('foods').snapshots();
  String searchString = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
              child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(15),
                child: Container(
                  child: TextField(
                    onChanged: (val) {
                      setState(() {
                        searchString = val;
                      });
                    },
                    controller: textEditingController,
                    decoration: InputDecoration(
                      hintText: 'Search foods here',
                      hintStyle: TextStyle(color: Colors.black),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: StreamBuilder(
                    stream: (searchString == null||searchString.trim() == '')
                        ? FirebaseFirestore.instance
                            .collection('foods')
                            .snapshots()
                        : FirebaseFirestore.instance
                            .collection('foods')
                            .where('searchIndex', arrayContains: searchString)
                            .snapshots(),
                    builder: (BuildContext context,
                        AsyncSnapshot<QuerySnapshot> snapshot) {
                      if (snapshot.hasError) {
                        return Text('Something went wrong');
                      }
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Text('Loading..');
                      }
                      switch (snapshot.connectionState) {
                        case ConnectionState.done:
                          return Text('We are done');
                        default:
                          return ListView(
                            children: snapshot.data!.docs
                                .map((DocumentSnapshot document) {
                              Map<String, dynamic> data =
                                  document.data()! as Map<String, dynamic>;
                              return Card(
                                child: Column(
                                  children: [
                                    ListTile(
                                      leading: Text(
                                        data['food_name'],
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 30),
                                      ),
                                      title: Text(
                                        data['food_calories'].toString() +
                                            " kcal",
                                        style: TextStyle(
                                            color: Colors.black,
                                            //fontWeight: FontWeight.bold,
                                            fontSize: 20),
                                      ),
                                      subtitle: Text(
                                        data['food_quantity'].toString() +
                                            " " +
                                            data['food_unit'],
                                        style: TextStyle(
                                            color: Colors.black,
                                            //fontWeight: FontWeight.bold,
                                            fontSize: 15),
                                      ),
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    foodSelectForm(
                                                      foodId: document.id,
                                                    )));
                                      },
                                    )
                                  ],
                                ),
                              );
                            }).toList(),
                          );
                      }
                    }),
              ),
            ],
          ))
        ],
      ),
    );
  }
}
