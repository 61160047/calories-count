import 'package:calories_count/foodsPage.dart';
import 'package:calories_count/dashboard.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'Calories Count',
    home: MyApp(),
  ));
}
