import 'dart:html';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class foodSelectForm extends StatefulWidget {
  String foodId;
  foodSelectForm({Key? key, required this.foodId}) : super(key: key);

  @override
  _foodSelectFormState createState() => _foodSelectFormState(foodId);
}

class _foodSelectFormState extends State<foodSelectForm> {
  String foodId;
  String foodName1 = "";
  int foodCalories1 = 0;
  String foodUnit1 = "";
  int foodQuantity1 = 0;
  int totalcnq = 0;
  var sum = [];
  CollectionReference foods = FirebaseFirestore.instance.collection('foods');
  CollectionReference activity =
      FirebaseFirestore.instance.collection('activity');
  _foodSelectFormState(this.foodId);
  TextEditingController _foodNameController = new TextEditingController();
  TextEditingController _foodCaloriesController = new TextEditingController();
  TextEditingController _foodUnitController = new TextEditingController();
  TextEditingController _foodQuantityController = new TextEditingController();

  void initState() {
    super.initState();
    if (this.foodId.isNotEmpty) {
      foods.doc(this.foodId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          setState(() {
            foodName1 = data['food_name'];
            foodCalories1 = data['food_calories'];
            foodUnit1 = data['food_unit'];
            foodQuantity1 = data['food_quantity'];
          });
          _foodNameController.text = foodName1;
          _foodCaloriesController.text = foodCalories1.toString();
          _foodUnitController.text = foodUnit1;
          _foodQuantityController.text = foodQuantity1.toString();
        }
      });
    }
  }

  final _formkey = GlobalKey<FormState>();

  Future<void> addFood() {
    return activity
        .add({
          'name': this.foodName1,
          'calories': this.foodCalories1,
          'quantity': this.foodQuantity1,
          'unit': this.foodUnit1,
        })
        .then((value) => print('food added'))
        .catchError((error) => print('Failed to add food: $error'));
  }

  /*Future<void> totalCalAndQuan() async {
    if (foodQuantity1 <= 9) {
      totalcnq = foodCalories1 * foodQuantity1;
      for(int i=0;sum.length>=0;i++){
        sum[i] += totalcnq;
        
      }
    } else {
      totalcnq = (foodCalories1 * (foodQuantity1 / foodQuantity1)) as int;
    }
    print(sum);
  }*/

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Food Select'),
      ),
      body: Form(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        key: _formkey,
        child: Container(
          padding: EdgeInsets.all(16),
          child: ListView(
            children: [
              TextFormField(
                controller: _foodNameController,
                //initialValue: fullname,
                decoration: InputDecoration(labelText: 'Food name'),

                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input food name';
                  }
                  return null;
                },
                onChanged: (value) {
                  setState(() {
                    foodName1 = value;
                  });
                },
              ),
              TextFormField(
                controller: _foodCaloriesController,
                //initialValue: fullname,
                decoration: InputDecoration(labelText: 'Food Calories'),

                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input food calories';
                  }
                  return null;
                },
                onChanged: (value) {
                  setState(() {
                    foodCalories1 = int.tryParse(value)!;
                  });
                },
              ),
              TextFormField(
                controller: _foodQuantityController,
                //initialValue: fullname,
                decoration: InputDecoration(labelText: 'Food quantity'),

                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input food quantity';
                  }
                  return null;
                },
                onChanged: (value) {
                  setState(() {
                    foodQuantity1 = int.tryParse(value)!;
                  });
                },
              ),
              TextFormField(
                controller: _foodUnitController,
                //initialValue: fullname,
                decoration: InputDecoration(labelText: 'Food unit'),

                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input food unit';
                  }
                  return null;
                },
                onChanged: (value) {
                  setState(() {
                    foodUnit1 = value;
                  });
                },
              ),
              ButtonBar(
                alignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text('Cancel')),
                  ElevatedButton(
                      onPressed: () async {
                        if (_formkey.currentState!.validate()) {
                          //if(foodId.isEmpty) {
                          await addFood();
                          //totalCalAndQuan();
                          //totalCalAndQuan();
                          //}

                          Navigator.pop(context);
                        }
                      },
                      child: const Text('Save')),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
