import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class foodAddForm extends StatefulWidget {
  String foodId;
  foodAddForm({Key? key, required this.foodId}) : super(key: key);

  @override
  _foodAddFormState createState() => _foodAddFormState(foodId);
}

class _foodAddFormState extends State<foodAddForm> {
  String foodId;
  String foodName = "";
  int foodCalories = 0;
  String foodUnit = "";
  int foodQuantity = 0;
  CollectionReference foods = FirebaseFirestore.instance.collection('foods');
  _foodAddFormState(this.foodId);
  TextEditingController _foodNameController = new TextEditingController();
  TextEditingController _foodCaloriesController = new TextEditingController();
  TextEditingController _foodUnitController = new TextEditingController();
  TextEditingController _foodQuantityController = new TextEditingController();
  
  final _formkey = GlobalKey<FormState>();

  Future<void> addFood() {
    List<String> splitList = foodName.split(' ');
    List<String> indexList = [];

    for(int i=0;i<splitList.length;i++){
      for(int j=0;j<splitList[i].length+i;j++){
        indexList.add(splitList[i].substring(0,j).toLowerCase());
      }
    }

    return foods
    .add({
      'food_name': this.foodName,
      'food_calories': this.foodCalories,
      'food_quantity': this.foodQuantity,
      'food_unit' : this.foodUnit,
      'searchIndex': indexList
    })
    .then((value) => print('food added'))
    .catchError((error) => print('Failed to add food: $error'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Add Food'),),
      body: Form(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        key: _formkey,
        child: Container(
          padding: EdgeInsets.all(16),
          child: ListView(
            children: [
              TextFormField(
                controller: _foodNameController,
                //initialValue: fullname,
                decoration: InputDecoration(labelText: 'Food name'),

                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input food name';
                  }
                  return null;
                },
                onChanged: (value) {
                  setState(() {
                    foodName = value;
                  });
                },
              ),
              TextFormField(
                controller: _foodCaloriesController,
                //initialValue: fullname,
                decoration: InputDecoration(labelText: 'Food Calories'),

                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input food calories';
                  }
                  return null;
                },
                onChanged: (value) {
                  setState(() {
                    foodCalories = int.tryParse(value)!;
                  });
                },
              ),
              TextFormField(
                controller: _foodQuantityController,
                //initialValue: fullname,
                decoration: InputDecoration(labelText: 'Food quantity'),

                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input food quantity';
                  }
                  return null;
                },
                onChanged: (value) {
                  setState(() {
                    foodQuantity = int.tryParse(value)!;
                  });
                },
              ),
              TextFormField(
                controller: _foodUnitController,
                //initialValue: fullname,
                decoration: InputDecoration(labelText: 'Food unit'),

                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input food unit';
                  }
                  return null;
                },
                onChanged: (value) {
                  setState(() {
                    foodUnit = value;
                  });
                },
              ),
              ButtonBar(
                alignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text('Cancel')),
                  ElevatedButton(
                      onPressed: () async {
                        if (_formkey.currentState!.validate()) {
                          //if(foodId.isEmpty) {
                          await addFood();
                          //totalCalAndQuan();
                          //}

                          Navigator.pop(context);
                        }
                      },
                      child: const Text('Save')),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}