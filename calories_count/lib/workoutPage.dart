import 'dart:html';

import 'package:calories_count/food_add_form.dart';
import 'package:calories_count/foods_information.dart';
import 'package:calories_count/dashboard.dart';
import 'package:calories_count/workout_add_form.dart';
import 'package:calories_count/workout_information.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class workoutPage extends StatefulWidget {
  workoutPage({Key? key}) : super(key: key);

  @override
  _workoutPageState createState() => _workoutPageState();
}

class _workoutPageState extends State<workoutPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Select Your Work out'),
      ),
      body: Container(
        child: workOutInformation(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          Navigator.push(context, MaterialPageRoute(builder: (context) => workoutAddForm(workoutId: '')));
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}